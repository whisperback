# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# snotree <cknblue@gmail.com>, 2017
# cwt96 <cwt967@naver.com>, 2012
# Johnny Cho <popeye92@gmail.com>, 2013
# sleepinium <lffs3659@gmail.com>, 2013
# 이피시소리 <pcsori@gmail.com>, 2012
# Philipp Sauter <qt123@pm.me>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-11 17:17+0200\n"
"PO-Revision-Date: 2018-11-14 21:05+0000\n"
"Last-Translator: Philipp Sauter <qt123@pm.me>\n"
"Language-Team: Korean (http://www.transifex.com/otf/torproject/language/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. XXX use a better exception
#: ../whisperBack/whisperback.py:56
#, python-format
msgid "Invalid contact email: %s"
msgstr "잘못된 이메일: %s"

#: ../whisperBack/whisperback.py:74
#, python-format
msgid "Invalid contact OpenPGP key: %s"
msgstr "잘못된 연락처 OpenPGP 키: %s"

#: ../whisperBack/whisperback.py:76
msgid "Invalid contact OpenPGP public key block"
msgstr "잘못된 연락처 OpenPGP 공증 키 차단"

#: ../whisperBack/exceptions.py:41
#, python-format
msgid ""
"The %s variable was not found in any of the configuration files "
"/etc/whisperback/config.py, ~/.whisperback/config.py, or ./config.py"
msgstr "변수 %s 를 설정 파일들(/etc/whisperback/config.py, ~/.whisperback/config.py, ./config.py)에서 찾을 수 없습니다."

#: ../whisperBack/gui.py:111
msgid "Name of the affected software"
msgstr "영향을 받는 소프트웨어의 이름"

#: ../whisperBack/gui.py:113
msgid "Exact steps to reproduce the error"
msgstr "문제를 재현하기 위한 정확한 단계"

#: ../whisperBack/gui.py:115
msgid "Actual result and description of the error"
msgstr "오류의 실제 결과 및 설명"

#: ../whisperBack/gui.py:117
msgid "Desired result"
msgstr "원하는 결과"

#: ../whisperBack/gui.py:130
msgid "Unable to load a valid configuration."
msgstr "올바른 설정을 불러올 수 없습니다."

#: ../whisperBack/gui.py:166
msgid "Sending mail..."
msgstr "메일 전송 중..."

#: ../whisperBack/gui.py:167
msgid "Sending mail"
msgstr "메일 전송 중"

#. pylint: disable=C0301
#: ../whisperBack/gui.py:169
msgid "This could take a while..."
msgstr "다소 시간이 걸릴 수 있습니다..."

#: ../whisperBack/gui.py:185
msgid "The contact email address doesn't seem valid."
msgstr "연락처 이메일 주소가 올바르지 않은 것을 같습니다."

#: ../whisperBack/gui.py:202
msgid "Unable to send the mail: SMTP error."
msgstr "메일을 보낼 수 없습니다: SMPT 오류."

#: ../whisperBack/gui.py:204
msgid "Unable to connect to the server."
msgstr "서버에 연결할 수 없습니다."

#: ../whisperBack/gui.py:206
msgid "Unable to create or to send the mail."
msgstr "메일을 작성하거나 보낼 수 없습니다."

#: ../whisperBack/gui.py:209
msgid ""
"\n"
"\n"
"The bug report could not be sent, likely due to network problems. Please try to reconnect to the network and click send again.\n"
"\n"
"If it does not work, you will be offered to save the bug report."
msgstr "\n\n네트워크 문제로 인해 버그 보고서를 보낼 수 없을 가능성이 있습니다. 네트워크를 다시 연결하고 다시 보내기를 시도하여 보십시오.\n\n작동하지 않는 경우, 버그 보고서를 저장하십시오.  "

#: ../whisperBack/gui.py:222
msgid "Your message has been sent."
msgstr "메시지를 보냈습니다."

#: ../whisperBack/gui.py:229
msgid "An error occured during encryption."
msgstr "암호화하는 동안 오류가 발생했습니다."

#: ../whisperBack/gui.py:249
#, python-format
msgid "Unable to save %s."
msgstr "%s을(를) 저장할 수 없습니다."

#: ../whisperBack/gui.py:272
#, python-format
msgid ""
"The bug report could not be sent, likely due to network problems.\n"
"\n"
"As a work-around you can save the bug report as a file on a USB drive and try to send it to us at %s from your email account using another system. Note that your bug report will not be anonymous when doing so unless you take further steps yourself (e.g. using Tor with a throw-away email account).\n"
"\n"
"Do you want to save the bug report to a file?"
msgstr "네트워크 문제로 인해 버그 보고서를 보낼 수 없을 가능성이 있습니다.\n\n버그 보고서를 파일로 USB 드라이브에 저장하여 다른 시스템을 사용하여 %s를 우리에게 보낼 수 있습니다. 참고: 추가 단계를 취하지 않으면 귀하의 버그 보고서는 익명으로 처리되지 않습니다.\n(예, 이메일 계정으로 토르를 사용하지 않는 경우)\n\n버그 보고서를 파일로 저장하시겠습니까?"

#: ../whisperBack/gui.py:332 ../data/whisperback.ui.h:21
msgid "WhisperBack"
msgstr "WhisperBack"

#: ../whisperBack/gui.py:333 ../data/whisperback.ui.h:2
msgid "Send feedback in an encrypted mail."
msgstr "암호화된 메일로 피드백을 보냅니다."

#: ../whisperBack/gui.py:336
msgid "Copyright © 2009-2018 Tails developers (tails@boum.org)"
msgstr "Copyright © 2009-2018 Tails developers (tails@boum.org)"

#: ../whisperBack/gui.py:337
msgid "Tails developers <tails@boum.org>"
msgstr "Tails developers <tails@boum.org>"

#: ../whisperBack/gui.py:338
msgid "translator-credits"
msgstr "번역-저작자"

#: ../whisperBack/gui.py:370
msgid "This doesn't seem to be a valid URL or OpenPGP key."
msgstr "URL 또는 OpenPGP키가 유효하지 않은 것 같습니다."

#: ../data/whisperback.ui.h:1
msgid "Copyright © 2009-2018 tails@boum.org"
msgstr "Copyright © 2009-2018 tails@boum.org"

#: ../data/whisperback.ui.h:3
msgid "https://tails.boum.org/"
msgstr "https://tails.boum.org/"

#: ../data/whisperback.ui.h:4
msgid ""
"WhisperBack - Send feedback in an encrypted mail\n"
"Copyright (C) 2009-2018 Tails developers <tails@boum.org>\n"
"\n"
"This program is  free software; you can redistribute  it and/or modify\n"
"it under the  terms of the GNU General Public  License as published by\n"
"the Free Software Foundation; either  version 3 of the License, or (at\n"
"your option) any later version.\n"
"\n"
"This program  is distributed in the  hope that it will  be useful, but\n"
"WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of\n"
"MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU\n"
"General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
msgstr "WhisperBack - 피드백을 암호화된 메일로 보낼수 있습니다.\nCopyright (C) 2009-2018 Tails developers <tails@boum.org>\n\n이 프로그램은 자유 소프트웨어입니다. 귀하는 이 프로그램을 자유 소프트웨어 재단에서 발행한 GNU General Public License 버전 3이나 그 이후의 버전으로 기여, 재생산, 배포 할 수 있습니다.\n\nThis program  is distributed in the  hope that it will  be useful, but\nWITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of\nMERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU\nGeneral Public License for more details.\n\nYou should have received a copy of the GNU General Public License\nalong with this program.  If not, see <http://www.gnu.org/licenses/>.\n"

#: ../data/whisperback.ui.h:20
msgid ""
"If you want us to encrypt messages when we respond to you, add your key ID, "
"a link to your key, or the key as a public key block:"
msgstr "우리가 당신에게 응답할 때 우리는 메시지를 암호화할 수 있는 귀하의 키 ID, 공개 키 블록과 같은 키에 대한 링크나 키를 추가합니다."

#: ../data/whisperback.ui.h:22
msgid "Summary"
msgstr "요약"

#: ../data/whisperback.ui.h:23
msgid "Bug description"
msgstr "버그 설명"

#: ../data/whisperback.ui.h:24
msgid "Help:"
msgstr "도움말:"

#: ../data/whisperback.ui.h:25
msgid "Read our bug reporting guidelines."
msgstr "저희 버그 리포트 가이드라인을 읽으십시오."

#: ../data/whisperback.ui.h:26
msgid "Email address (if you want an answer from us)"
msgstr "이메일 주소 (저희 대답을 받고 싶으면 기재하십시오)"

#: ../data/whisperback.ui.h:27
msgid "optional PGP key"
msgstr "부가적인 PGP 키"

#: ../data/whisperback.ui.h:28
msgid "Technical details to include"
msgstr "기술적인 세부 사항 포함"

#: ../data/whisperback.ui.h:29
msgid "headers"
msgstr "헤더"

#: ../data/whisperback.ui.h:30
msgid "debugging info"
msgstr "디버깅 정보"

#: ../data/whisperback.ui.h:31
msgid "Send"
msgstr "보내기"
